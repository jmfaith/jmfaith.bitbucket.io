<?php
// define variables and set to empty values
$firstNameErr = $lastNameErr= $myEmailErr = $myMessageErr = "";
$firstName = $lastName = $myEmail = $myMessage =  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["firstName"])) {
    $firstNameErr = "First Name is required";
  } else {
    $firstName = test_input($_POST["firstName"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
      $firstNameErr = "Only letters and white space allowed";
    }
  }
     if (empty($_POST["lastName"])) {
    $lastNameErr = "Last Name is required";
  } else {
    $lastName = test_input($_POST["lastName"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$lastName)) {
      $lastNameErr = "Only letters and white space allowed";
    }
  }

  if (empty($_POST["myEmail"])) {
    $myEmailErr = "eMail address is required";
  } else {
    $myEmail = test_input($_POST["myEmail"]);
    // check if eMail address is well-formed
    if (!filter_var($myEmail, FILTER_VALIDATE_EMAIL)) {
      $myEmailErr = "Invalid eMail address format";
    }
  }

  if (empty($_POST["myMessage"])) {
    $myMessage = "";
  } else {
    $myMessage = test_input($_POST["myMessage"]);
  }

  /*if (empty($_POST["radio"])) {
    $radioErr = "Yes or No is required";
  } else {
    $radio = test_input($_POST["radio"]);
  }*/
}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Judy Faith | Resume</title>
  <meta name="description" content="A responsive Mobile Web Development resume page using Skeleton - Judy Faith">
  <meta name="author" content="Judy Faith">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!--<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">-->
     <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    


  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

     <!-- FONT AWESOME 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">
<style>
    body { background-color: #EEEEEE; 
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    background: linear-gradient(teal, white); /* Standard syntax (must be last) */
}
 #grad1 {
    height: 3000px;
    width: 100%; 
    padding: 15px;
        
    background: red; /* For browsers that do not support gradients */
    background: linear-gradient(teal, white); /* Standard syntax (must be last) */
}
a {
    color: darkblue;
}  
    
    .center {
      
      margin: 0 auto;
      
    }/* css font color change code by Rasin Zakaria https://codepen.io/raaasin/#*/
    
    .awesome {
      font-family: 'Open Sans', sans-serif;
      font-style: normal;
      
      width:100%;
      padding-bottom: 0%;
      
      margin: 0 auto;
      text-align: center;
      
      color:#313131;
      font-size:45px;
      font-weight: bold;
      position: relative;
      -webkit-animation:colorchange 70s infinite alternate;
      
      
    }

    @-webkit-keyframes colorchange {
      0% {
        
        color: #fff;
      }
      
      10% {
        
        color: #8e44ad;
      }
      
      20% {
        
        color: #1abc9c;
      }
      
      30% {
        
        color: #d35400;
      }
      
      40% {
        
        color: #fff;
      }
      
      50% {
        
        color: #34495e;
      }
      
      60% {
        
        color: #fffccc;
      }
      
      70% {
        
        color: #2980b9;
      }
      80% {
     
        color: #f1c40f;
      }
      
      90% {
     
        color: #2980b9;
      }
      
      100% {
        
        color: pink;
      }
    }
.captitle {
    background-color: #cc0066;
    color: white;
    padding: 10px;
    color: #CEFCFC;
} 
.captitle2 {
    background-color: #FFFFFF;
    color: black;
    padding: 10px;
    color: #000000;
    text-align: center
} 
    /*.greybubble {
    background-color: darkgray;
}*/
    .emailInput {
    }
    font { 
     font-size: 16px; font-style: normal; font-family: sans-serif; 
    }
    h1 {
    color: floralwhite;
    /*color: rgba(20, 100, 50, .9);
    /*text-shadow: -4px 4px 3px #666, 1 -1px 2px #000;*/
    font-size: 300%; 
    text-align: center;
    /*-webkit-animation-name: example; /* Safari 4.0 - 8.0 */
   /*-webkit-animation-duration: 8s; /* Safari 4.0 - 8.0 */
    /*animation-name: example;
    animation-duration: 8s;
        animation-direction: alternate-reverse;*/
    }
    /*div1 {
    width: 100px;
    height: 100px;
    background-color: red;
    -webkit-animation-name: example; /* Safari 4.0 - 8.0 */
   /* -webkit-animation-duration: 4s; /* Safari 4.0 - 8.0 */
    /* animation-name: example;
    animation-duration: 4s;
}

/* Safari 4.0 - 8.0 */
/* @-webkit-keyframes example {
    from {color: floralwhite;}
    to {color: teal;}

/* Standard syntax */
/* @keyframes example {
    from {color: floralwhite;}
    to {color: teal;
} */
    
    h2 {
        font-size: 210%; 
    }
    h3 {
        font-size: 170%; 
    }
    
     h4 {
        font-size: 150%;
        color: darkslategrey;
       
    }
     h5 {
        font-size: 130%;
        color: darkslategrey;
    } 
    h6 {
        font-size: 120%; 
        color: darkslategrey;
    }
img {
max-width: 100%
}
img.center {
display: block;
margin-left: auto;
margin-right: auto;
}
#example-two {
  border-radius: 10px;
  border: 3px solid #BADA55;
}
#border-radius-40 { 
 -webkit-border-radius: 40px;
 -moz-border-radius: 40px;
 border-radius: 40px;
    }
.shadow {
  -moz-box-shadow:    0px 8px 45px -15px rgba(0,0,0,1);
  -webkit-box-shadow: 0px 8px 45px -15px rgba(0,0,0,1);
  box-shadow:         0px 8px 45px -15px rgba(0,0,0,1);
 
   /* -webkit-box-shadow: 0px 3px 100px -10px rgba(0,0,0,1);
    -moz-box-shadow: 0px 3px 100px -10px rgba(0,0,0,1);
     box-shadow: 0px 3px 100px -10px rgba(0,0,0,1); */
}
    #example1 {
    border: 1px solid;
    padding: 10px;
    background: linear-gradient(powderblue, white);
}
  #example2 {
    border: 1px solid;
    padding: 10px;
    background: linear-gradient(powderblue, white);
}
 #example3 {
    border: 1px solid;
    padding: 10px;
    background: linear-gradient(powderblue, white);
} #example4 {
    border: 1px solid;
    padding: 10px;
    background: linear-gradient(powderblue, white);
}
 #example5 {
    border: 1px solid;
    padding: 10px;
    background: linear-gradient(powderblue, white);
}
 #example6 {
    border: 1px solid;
    padding: 10px;
    background: linear-gradient(powderblue, white);
}
 #example7 {
    border: 1px solid;
    padding: 10px;
    background: linear-gradient(powderblue, white);
}
    .accordion {
    background-color: #cc0066;
    cursor: pointer;
    padding: 10px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 150%;
    transition: 0.4s;
}
    .active, .accordion:hover {
    background: linear-gradient(powderblue, white);
    color: black;
}

.panel {
    display: none;
    background-color: white;
    overflow: hidden;
}
.accordion:after {
    content: '\002B';
    color: floralwhite;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}


.active:after {
    content: "\2212";
    color: black;
}
.error {
color: #FF0000;
}  
</style>
</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div id="grad1" class="container" >
      <div class="row">
      <div class="twelve columns">
        <!--<nav>
          <ul>
            <li><a href="#">About</a></li>
            <li><a href="#">Education</a></li>
            <li><a href="#">Skills</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </nav>  
      </div>-->
    </div>
    <div class="row">
      <div class="12 columns" style="margin-top: 0%">
      <div class="row">
    
      <div class="10 columns" style="margin-top: 2%">
         <img src="images/judy_faith-400x400-15.jpg" class="center" id="example-two" alt="a picture of Judy Faith">
          <h1 class="awesome"><strong>JUDY  FAITH</strong></h1> </div>
          </div>
  <div class="row">
    
      <div class="10 columns" style="margin-top: 2%">
        <h6 class="captitle2 shadow" id="border-radius-40">Curious, liberal minded, creative, web developer seeks a venue to grow newly acquired talents</h6>
          </div>
          <!--<div class="greybubble"><pre><code>Curious, liberal minded, creative web artist seeks a venue to grow newly acquired talents</code></pre>  </div>
          
      </div> -->
</div>
<div class="row">
   
    <div class="12 columns" style="margin-top: 0%">
          <!--<div class="shadow">

-->
        <h4 class="10 columns captitle accordion">EDUCATION</h4>
         <div id="example1" class="panel">         
            <div class="row">
            <div class="six columns" style="margin-top: 0%">
            <h5><i class="fas fa-cog"></i> WEB DEVELOPER</h5>
            <a href="https://www.dccc.edu/" target="_blank">Delaware County Community College</a>
            <br>Media, PA, Dec 2018  
            </div>
    <div class="six columns" style="margin-top: 0%">

    <h5><i class="fas fa-compass"></i> WEBMASTER</h5>
   <a href="https://www.udel.edu/" target="_blank">University of Delaware</a>
   <br>Newark, DE, May 2000
    </div>
            </div>
           <div class="row">
          <div class="six columns" style="margin-top: 5%">
    <h5><i class="fas fa-university"></i> BACHELOR OF FINE ARTS</h5> 
    
<a href="https://tyler.temple.edu/#/prospective" target="_blank">Tyler School of Art</a>
<br>Temple University
<br>Philadelphia, PA, May 1981
           </div> 
               </div>
    <div class="row">
        <div class="twelve columns" style="margin-top: 5%">
          <h6><i class="fas fa-globe"></i> CERTIFIED CLINICAL RESEARCH ASSOCIATE</h6>
<a href="https://www.acrpnet.org/" target="_blank">Association of Clinical Research Professionals</a>
<br>San Diego, CA, Mar 2008 - present (active 10 yrs)
        </div>
            </div>
    </div>
          </div>
    </div>
          </div>
</div>
      <div class="row">
    <div class="10 columns" style="margin-top: 2%">
        
        <h4 class="10 columns captitle accordion">SKILLS - CODING</h4>
  <div id="example2" class="panel">        
<div class="row">
    <div class="row">
  <div class="six columns">
        <h5><i class="fas fa-bookmark"></i> HTML5 </h5>
        <h5><i class="fas fa-bookmark"></i> JavaScript </h5>
        <h5><i class="fas fa-bookmark"></i> CSS3 </h5>
        <h5><i class="fas fa-bookmark"></i> JQuery </h5>
         </div>
    <div class="six columns">
        <h5><i class="fas fa-bookmark"></i> PHP </h5>
        <h5><i class="fas fa-bookmark"></i> MySQL </h5>
        <h5><i class="fas fa-bookmark"></i> JAVA </h5>
        <h5><i class="fas fa-bookmark"></i> LINUX </h5>
    </div>
    </div>
    </div>
    </div>
</div>
          
          <div class="row">
      <div class="10 columns" style="margin-top: 2%">
          
        <h4 class="10 columns captitle accordion">SKILLS - GRAPHIC</h4>
 <div id="example3" class="panel">         
<div class="row">
    <div class="row">
    <div class="six columns">
        <h5><i class="fas fa-bookmark"></i><a href="https://en.wikipedia.org/wiki/Digital_photography" target="_blank"> DIGITAL PHOTOGRAPHY </a></h5>
        <h5><i class="fas fa-bookmark"></i><a href="https://www.adobe.com/products/photoshop.html" target="_blank">  ADOBE PHOTOSHOP </a></h5>
    </div>
        <div class="six columns">
        
        <h5><i class="fas fa-bookmark"></i><a href="https://www.adobe.com/products/photoshop-lightroom-classic.html" target="_blank"> ADOBE LIGHTROOM </a></h5>
        <h5><i class="fas fa-bookmark"></i> <a href="https://www.adobe.com/products/dreamweaver.html" target="_blank"> ADOBE DREAMWEAVER</a> </h5>
         </div>
    </div>
</div>
              </div>
                 <div class="row">
      <div class="10 columns" style="margin-top: 2%">
          
        <h4 class="10 columns captitle accordion">SKILLS - OFFICE</h4>
 <div id="example4" class="panel">         
<div class="row">
    <div class="row">
   
        <div class="six columns">
        <h5><i class="fas fa-bookmark"></i> <a href="https://acrobat.adobe.com/us/en/acrobat.html?promoid=DMMD1FLC&mv=other" target="_blank">ADOBE Acrobat</a> </h5>
        <h5><i class="fas fa-bookmark"></i> <a href="https://www.webex.com/" target="_blank"> CISCO WebEx</a> </h5>
        <h5><i class="fas fa-bookmark"></i><a href="https://products.office.com/en-us/outlook/email-and-calendar-software-microsoft-outlook?tab=tabs-1" target="_blank"> MICROSOFT Outlook</a> </h5>
         </div>
         <div class="six columns">
        <h5><i class="fas fa-bookmark"></i> <a href="https://products.office.com/en-us/excel" target="_blank">MICROSOFT Excel</a></h5>
        <h5><i class="fas fa-bookmark"></i> <a href="https://products.office.com/en-us/powerpoint" target="_blank">MICROSOFT PowerPoint</a>  </h5>
        <h5><i class="fas fa-bookmark"></i> <a href="https://support.office.com/en-us/article/What-is-SharePoint-97b915e6-651b-43b2-827d-fb25777f446f" target="_blank"> MICROSOFT SharePoint </a> </h5>
    </div>
    </div>
</div>
              </div>
<div class="row">
      <div class="10 columns" style="margin-top: 2%">
          
        <h4 class="10 columns captitle accordion">CLINICAL TECHNOLOGY</h4>
          <div id="example5" class="panel">
<div class="row">
    <div class="six columns">   
        <h5><i class="fas fa-bookmark"></i><a href="https://www.parexel.com/solutions/informatics/education-services/impact-ctms-training" target="_blank"> IMPACT&copy; CTMS</a> </h5>
        <h5><i class="fas fa-bookmark"></i><a href="http://www.oracle.com/us/products/applications/health-sciences/e-clinical/inform/index.html" target="_blank"> ORACLE InFORM</a>  </h5>
        <h5><i class="fas fa-bookmark"></i><a href="https://www.mdsol.com/en/products/rave" target="_blank"> MEDIDATA RAVE</a> </h5>   
         
    </div>
        <div class="six columns">
        <h5><i class="fas fa-bookmark"></i> <a href="https://www.almacgroup.com/clinical-technologies/ixrs3-interactive-response-technology/" target="_blank">ALMAC IXRS </a> </h5> 
        <h5><i class="fas fa-bookmark"></i> <a href="https://www.veeva.com/products/enterprise-content-management/" target="_blank">VEEVA VAULT eTMF</a> </h5>
        <!--<h5><i class="fas fa-bookmark"></i> Something </h5>
        <h5><i class="fas fa-bookmark"></i><a href="https://www.ert.com/safety-efficacy/respiratory/" target="_blank"> ERT Respiratory Training</a> </h5>-->
        </div>
         </div>
              </div>
 
          <div class="row">
      <div class="10 columns" style="margin-top: 2%">
          
        <h4 class="10 columns captitle accordion">WORK EXPERIENCE</h4>
         <div id="example6" class="panel"> 
<div class="row">
    <div class="row">
    <div class="nine columns">
    
        <h5><i class="fas fa-globe"></i> SENIOR CLINICAL RESEARCH ASSOCIATE</h5>
        <ul>
            <li>  2014 - 2017 - <a href="https://prahs.com/" target="_blank">PRA HEALTH SCIENCES</a> </li>
            <li>  2007 - 2014 - <a href="https://www.iqvia.com/" target="_blank">QUINTILES (now IQVIA)</a></li>
            <li>  2006 - 2007 - <a href="https://www.ppdi.com/" target="_blank">PHARMACEUTICAL PRODUCT DEVELOPMENT (PPD)</a></li> 
        </ul>
        
         <h5><i class="fas fa-globe"></i> CLINICAL RESEARCH ASSOCIATE
         </h5>
        
        <ul>
            <li> 2005 - 2006 - <a href="https://www.hemispherx.net/" target="_blank">HEMISPHERX BIOPHARMA</a></li>
        </ul>
        <h5><i class="fas fa-globe"></i> CLINICAL ADMINISTRATIVE COORDINATOR</h5>
        <ul>
            <li> 2001 - 2005 - <a href="https://www.astrazeneca.com/" target="_blank">ASTRAZENECA PHARMACEUTICALS</a> </li>
            </ul>
        <!--<h5><i class="fas fa-bookmark"></i> MySQL </h5>
        <h5><i class="fas fa-bookmark"></i> JAVA </h5>
        <h5><i class="fas fa-bookmark"></i> LINUX </h5>
        <h5><i class="fas fa-bookmark"></i> ADOBE CC </h5>
        <h5><i class="fas fa-bookmark"></i> IMPACT CTMS </h5> -->
     
        </div>
         </div>
  </div>
    </div>
              </div>

<!-- Contact Form
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <div class="row">
      <div class="10 columns" style="margin-top: 2%">
        <h4 class="10 columns captitle accordion">CONTACT ME</h4>
          <div id="example7" class="panel"> 
 <!--<form action="report.php" method="post" id="myForm">-->
<form method="post"  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF#contact"]);?>"> 
    
   
    <div class="row">
 <div class="six columns" >
<div id="contact">
   
<label for="firstName">First Name<span class="error">* <?php echo $firstNameErr;?></span></label>
        <input  type="text" class="u-full-width" placeholder="Your First Name" name="firstName" id="firstName" value="<?php if (isset($_POST['firstName'])) echo $_POST['firstName']; ?>">

<label for="lastName">Last Name<span class="error">* <?php echo $lastNameErr;?></span></label>
        <input type="text" class="u-full-width" placeholder="Your Last Name" name="lastName" id="lastName" value="<?php if (isset($_POST['lastName'])) echo $_POST['lastName']; ?>">

<label for="myEmail">Your email<span class="error">* <?php echo $myEmailErr;?></span></label>
      <input type="email" class="u-full-width" placeholder="youremail@mailbox.com" name="myEmail" id="myEmail" value="<?php if (isset($_POST['myEmail'])) echo $_POST['myEmail']; ?>">

<label for="myMessage">Message</label>
<input type="text" placeholder="Your Message" name="myMessage" class="u-full-width" value="<?php if (isset($_POST['myMessage'])) echo $_POST['myMessage']; ?>">

<!--<label for="recipientInput">Reason for contacting</label>
      <select class="u-full-width" name="recipientInput" id="recipientInput" value="<?php if (isset($_POST['recipientInput'])) echo $_POST['recipientInput']; ?>">
        <option value="1" <?php if (isset($_POST['recipientInput']) && ($_POST['recipientInput'] == '1')) echo ' selected="selected"'; ?>>Questions</option>
        <option value="2" <?php if (isset($_POST['recipientInput']) && ($_POST['recipientInput'] == '2')) echo ' selected="selected"'; ?>>Admiration</option>
        <option value="3" <?php if (isset($_POST['recipientInput']) && ($_POST['recipientInput'] == '3')) echo ' selected="selected"'; ?>>Networking</option>
        <option value="4" <?php if (isset($_POST['recipientInput']) && ($_POST['recipientInput'] == '4')) echo ' selected="selected"'; ?>>Can I get your number?</option>
        <option value="5" <?php if (isset($_POST['recipientInput']) && ($_POST['recipientInput'] == '5')) echo ' selected="selected"'; ?>>Are you available for an interview?</option>
      </select>-->
<!--</div><div class="six columns">--> 
           
<br><span class="error">* Required Fields</span>
<br>
<input type="submit" value="Submit">
<input type="reset" value="Reset">
<br>
     
<p> 
<?php
echo "<h5>Your Input</h5>";
echo $firstName; 
echo "<br>";
echo $lastName;
echo "<br>";
echo $myEmail;
echo "<br>";
echo $myMessage;
echo $recipientInput;
echo "<br>";
$myStory = $_POST['myStory'];
            $to = 'judith@judyfaith.com';
            $subject = "Resume - Contact Form Submission";
            $body = "Name: $firstName $lastName \n
            eMail Address: $myEmail \n
            Message: $myMessage \n"; 
            
            mail($to, $subject, $body);
?>
</p> 
     
</div>
        </div>
</div>    
</form>
        <footer>
            <br>
<small><i>Copyright &copy; 2018 Judy Faith <br>
</i></small>
</footer>
        <br>

        </div>
              
    </div>
                     </div>
              </div>
          </div>
        </div>
          </div>
      </div>
    </div>
          </div>
          </div>
          </div>
        </div>
    
  
      <!--<div class="offset-by-two eight columns">
        <ul>
          <li><a href="#" title=""><span class="fa fa-facebook"></span></a></li>
          <li><a href="#" title=""><span class="fa fa-twitter"></span></a></li>
          <li><a href="#" title=""><span class="fa fa-dribbble"></span></a></li>
          <li><a href="#" title=""><span class="fa fa-linkedin"></span></a></li>
          <li><a href="#" title=""><span class="fa fa-github"></span></a></li>
        </ul>
      </div>-->
<!--end of .row-->
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– --><script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
</script>
    
</body>
</html>
